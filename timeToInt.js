module.exports =  function(time){
    var chunks = time.split(':');
    if(chunks.length === 1) return Math.floor(Number(chunks[0]));
    if(chunks.length === 2) return Math.floor(Number(chunks[0]) * 60 + Number(chunks[1]));
    if(chunks.length === 3) return Math.floor(Number(chunks[0]) * 3600 + Number(chunks[1]) * 60 + Number(chunks[2]));
    throw new Error('Invalid time format.')
  }